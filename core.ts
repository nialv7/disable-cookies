/// <reference path="./typings/index.d.ts" />

class Prefs {
	constructor(public showContextMenu: boolean, public autoRefresh: boolean) {}
	static fromJSON(json: string) {
		let tmp = JSON.parse(json);
		return new Prefs(tmp.showContextMenu, tmp.autoRefresh);
	}
	toJSON() : string {
		return JSON.stringify(this);
	}
}

class Rule {
	constructor(public pattern: string, public policy: string, public uuid?: string) {}
}

let prefs : Prefs;
let currentVersion = chrome.runtime.getManifest().version;
let forbidden_origin = /^chrome:\/\//g;
let hostname_regex = new RegExp('^(?:f|ht)tp(?:s)?://([^/]+)', 'im');
let curr_key = null;
function _readStorage(key: string) {
	return new Promise<any>((resolve, reject) => {
		chrome.storage.sync.get(key, (obj) => {
			if (chrome.runtime.lastError)
				reject(chrome.runtime.lastError);
			else
				resolve(obj);
		});
	});
}

function _writeStorage(obj: any) {
	return new Promise<void>((resolve, reject) => {
		chrome.storage.sync.set(obj, () => {
			if (chrome.runtime.lastError)
				reject(chrome.runtime.lastError);
			else
				resolve();
		});
	});
}

function clearRules() {
	chrome.storage.sync.clear();
}

async function reloadRules() {
	await new Promise((resolve, reject) =>
		chrome.contentSettings.cookies.clear({scope: 'regular'}, resolve));
	importRules();
}

async function importRules() {
	let rules = await getStoredRules();
	for(let r of rules) {
		chrome.contentSettings.cookies.set({
			'primaryPattern': r.pattern,
			'setting': r.policy,
			'scope': 'regular'
		});
	}
}

function getCookieSettings(url: string, incognito: boolean) {
	return new Promise<chrome.contentSettings.ReturnedDetails>(
	(resolve, reject) => chrome.contentSettings.cookies.get({
		'primaryUrl': url,
		'incognito': incognito
	}, resolve));
}

function tabQuery(q: any) {
	return new Promise<chrome.tabs.Tab[]>((resolve, reject) => chrome.tabs.query(q, resolve));
}
// Convert a hex string to a byte array
function hex2ab(hex: string) : Uint8Array{
	let bytes = new Uint8Array(hex.length/2);
	for (let c = 0; c < hex.length; c += 2)
		bytes[c/2] = parseInt(hex.substr(c, 2), 16);
	return bytes;
}

// Convert a byte array to a hex string
function ab2hex(bytes: Uint8Array) : string {
	let hex = [];
	for (let i = 0; i < bytes.length; i++) {
		hex.push((bytes[i] >>> 4).toString(16));
		hex.push((bytes[i] & 0xf).toString(16));
	}
	return hex.join("");
}

function str2ab(str: string) : Uint8Array {
	return new Uint8Array(str.split("").map(c => c.charCodeAt(0)));
}

function ab2str(ab: Uint8Array) : string {
	return String.fromCharCode.apply(null, ab);
}

async function decryptRule(rule: any, uuid?: string) {
	if (curr_key !== null) {
		if (!("iv" in rule))
			return null;
		let iv = hex2ab(rule.iv);
		let pattern = await window.crypto.subtle.decrypt(
			<any>{name: "AES-GCM",iv: iv},
			curr_key,
			hex2ab(rule.pattern)
		);
		return new Rule(ab2str(new Uint8Array(pattern)), rule.policy, uuid);
	} else {
		if (/:\/\//.test(rule.pattern))
			return new Rule(rule.pattern, rule.policy, uuid);
		else
			throw "Decryptiong Failed";
	}
}

async function getStoredRules() {
	let _rules = await _readStorage(null), rules : Rule[] = [];
	for (let k in _rules) {
		if (/^cookie_rule:\/\//.test(k)) {
			let r = await decryptRule(_rules[k], k.substr(14));
			if (r !== null)
				rules.push(r);
			else
				chrome.storage.sync.remove(k);
		}
	}
	return rules;
}

async function updateIcon(tab?: chrome.tabs.Tab) {
	if (!tab) {
		tab = (await tabQuery({active: true, currentWindow: true}))[0];
		if (!tab)
			//Not focusing on a tab
			return;
	}
	let match_forbidden_origin = tab.url ? tab.url.match(forbidden_origin) : true;
	if (match_forbidden_origin)
		chrome.browserAction.setIcon({path: "icon-inactive.png"});
	else {
		let cookie_settings = await getCookieSettings(tab.url, tab.incognito);
		chrome.browserAction.setIcon({path: `icon-${cookie_settings.setting}.png`});
	}
}

async function changeSettings() {
	await loadKey();
	let tab = (await tabQuery({active: true, currentWindow: true}))[0];
	let match_forbidden_origin = tab.url ? tab.url.match(forbidden_origin) : true;
	if (!match_forbidden_origin) {
		let cookie_settings = await getCookieSettings(tab.url, tab.incognito);
		let pattern;
		if (/^file:/.test(tab.url))
			pattern = tab.url;
		else
			pattern = tab.url.match(hostname_regex)[0]+'/*';

		let new_setting = (cookie_settings.setting == 'allow') ? 'block' : 'allow';

		if (!tab.incognito)
			storeRules([new Rule(pattern, new_setting)]);

		//storage onChanged handler should handle the contentSettings change
		//But we want the setting applied NOW
		chrome.contentSettings.cookies.set({
			'primaryPattern': pattern,
			'setting': new_setting,
			'scope': tab.incognito ? 'incognito_session_only' : 'regular'
		}, () => {
			if (tab.id)
				chrome.tabs.reload(tab.id, {bypassCache: true});
		});
	}
}

async function storeRules(rules: Rule[]) {
	let entry = {};
	for (let r of rules) {
		let hash_str : string;
		if (r.uuid)
			hash_str = r.uuid;
		else {
			let hash = await window.crypto.subtle.digest(
				{name: "SHA-256"},
				str2ab(r.pattern)
			);

			hash_str = ab2hex(new Uint8Array(hash));
		}
		if (curr_key === null)
			entry[`cookie_rule://${hash_str}`] = {pattern: r.pattern, policy: r.policy};
		else {
			let iv = <Uint8Array>window.crypto.getRandomValues(new Uint8Array(12));
			let enc_pattern = await window.crypto.subtle.encrypt(
				<any>{name: "AES-GCM", iv: iv},
				curr_key,
				str2ab(r.pattern)
			);
			entry[`cookie_rule://${hash_str}`] = {
				pattern: ab2hex(new Uint8Array(enc_pattern)),
				policy: r.policy,
				iv: ab2hex(iv)
			};
		}
	}
	_writeStorage(entry);
}

async function updateRule(obj: { [key: string] : chrome.storage.StorageChange}) {
	for(let k in obj) {
		if (!/^cookie_rule:\/\//.test(k))
			continue;
		if (obj[k].newValue === undefined) {
			//Some rule is removed, we have to clear all and reload
			//Because chrome.contentSettings doesn't have remove()
			reloadRules();
			return;
		}
	}
	//There's no rule removed
	//Just apply new settings
	for(let k in obj) {
		if (!/^cookie_rule:\/\//.test(k))
			continue;
		let rule : Rule = await decryptRule(obj[k].newValue);
		chrome.contentSettings.cookies.set({
			'primaryPattern': rule.pattern,
			'setting': rule.policy,
			'scope': 'regular'
		});
	}
	updateIcon();
}

async function loadKey() {
	let _key = await new Promise<any>((resolve, reject) => chrome.storage.local.get("enckey", resolve)), key = null;
	if ("enckey" in _key)
		key = _key.enckey;

	if (key)
		curr_key = await window.crypto.subtle.importKey(
			"jwk", key, {name: "AES-GCM"},false,["encrypt", "decrypt"]
		);
	else
		curr_key = null;
}

async function setKey(key: any) {
	try {
		let old_rules = await getStoredRules();

		if (key)
			curr_key = await window.crypto.subtle.importKey(
				"jwk", key, {name: "AES-GCM"},false,["encrypt", "decrypt"]
			);
		else
			curr_key = null;
		storeRules(old_rules);
	} catch(e) {
		if (key)
			curr_key = await window.crypto.subtle.importKey(
				"jwk", key, {name: "AES-GCM"},false,["encrypt", "decrypt"]
			);
		else
			curr_key = null;
	}
	//Finally save the key to storage.local and signal
	//option page that we have finished
	if (key)
		chrome.storage.local.set({enckey: key});
	else
		chrome.storage.local.remove("enckey");
}

chrome.runtime.onInstalled.addListener(async () => {
	await loadKey();
	importRules();
	chrome.storage.local.set({"version": currentVersion});
});

chrome.browserAction.onClicked.addListener(changeSettings);
chrome.storage.onChanged.addListener(async (obj, area) => {
	await loadKey();
	if (area === "sync")
		updateRule(obj);
});
chrome.runtime.onMessage.addListener((req : any, sender, res) => {
	loadKey()
	.then(() => {
		if (req.action === "getRules") {
			getStoredRules()
			.then((rs) => res({action: "getRules", rules: rs}))
			.catch((err) => res({action: "getRules", rules: null}));
		} else {
			if (req.action === "setKey")
				setKey(req.key);
			else if (req.action === "clearRules")
				clearRules();
			else if (req.action === "reloadRules")
				reloadRules();
			else if (req.action === "addRules")
				storeRules(req.rules);
		}
	})
	.catch((err) => res({action: req.action, err: err}));
	return true;
});

chrome.tabs.onUpdated.addListener((tabId, props, tab) => {
	if (props.status == "loading" && tab.selected)
		updateIcon(tab);
});

chrome.tabs.onHighlighted.addListener(() => updateIcon());
chrome.windows.onFocusChanged.addListener(() => updateIcon());

chrome.windows.getCurrent(async (win) => {
	let tab = (await tabQuery({windowId: win.id, active: true}))[0];
	updateIcon(tab);
});
