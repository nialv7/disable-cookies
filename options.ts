/// <reference path="./typings/index.d.ts" />
chrome.storage.onChanged.addListener((key, area) => {
	showRules();
})

async function showRules() {
	let res = await new Promise<any>((resolve, reject) => {
		chrome.runtime.sendMessage({action: "getRules"}, function(res) {
			console.log(res);
			resolve(res);
		});
	});
	if (res.rules !== null) {
		for(let r of res.rules)
			delete r.uuid;
		(<HTMLInputElement>document.getElementById("cookiesTF_rules")).value = JSON.stringify(res.rules);
	} else {
		(<HTMLInputElement>document.getElementById("cookiesTF_rules")).value = "Failed to load stored rules (wrong password?)";
	}
}
function str2ab(str: string) : Uint8Array {
	return new Uint8Array(str.split("").map(c => c.charCodeAt(0)));
}
let salt = str2ab("Mary had a little lamb");
window.onload = function() {
	showRules();
	chrome.storage.local.get("enckey", (obj) => {
		if ("enckey" in obj) {
			let e = (<HTMLInputElement>document.getElementById("enckey"));
			e.value = "**********";
			e["placeholder_value"] = true;
		}
	});
	document.getElementById("clearCookieSettings").onclick = async () => {
		chrome.runtime.sendMessage({action: "clearRules"});
	};

	document.getElementById("reloadRules").onclick = () => {
		chrome.runtime.sendMessage({action: "reloadRules"});
	};
	document.getElementById("enckey").onfocus = () => {
		let e = (<HTMLInputElement>document.getElementById("enckey"));
		if (e["placeholder_value"]) {
			e.value = "";
			e["placeholder_value"] = false;
		}
	};
	document.getElementById("enckeyApply").onclick = async () => {
		let e = <HTMLInputElement>document.getElementById("enckey");
		if (e["placeholder_value"])
			return;
		let passwd = e.value;
		if (passwd === "") {
			chrome.runtime.sendMessage({action: "setKey", key: null});
			return;
		}
		let key = await window.crypto.subtle.importKey(
			"raw", str2ab(passwd), {name: "PBKDF2"}, true, ["deriveKey"]
		);
		let dkey = await window.crypto.subtle.deriveKey(
			<any>{name: "PBKDF2", iterations: 10000, salt: salt, hash: {name: "SHA-256"}},
			key,
			<any>{name: "AES-GCM", length: 256},
			true,
			['encrypt', 'decrypt']
		);
		let ekey = await window.crypto.subtle.exportKey(
			"jwk", dkey
		);
		chrome.runtime.sendMessage({action: "setKey", key: ekey});
	}

	document.getElementById("importRules").onclick = () => {
		let tmp = (<HTMLInputElement>document.getElementById("cookiesTF_rules")).value;
		if (tmp !== "") {
			let rules = JSON.parse(tmp);
			chrome.runtime.sendMessage({action: "addRules", rules: rules});
		}
	};
};
